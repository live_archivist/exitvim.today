---
layout: default
---

So you were following a coding tutorial and _somehow_ you got stuck in `vi` or `vim`.

Have no fear, you too can exit `VI/VIM` today.

1. Hit your `esc` key, to enter NORMAL mode
2. Type a colon `:`, putting you in Command Line Mode
3. Type `wq` to save, `:q!` to exit without saving

_psst... There's other ways to exit `vim` too, for the pros in the room:_

```
:q - "Please Quit, I already saved."
:wq! - "Save and quit, damnit!"
:x - "Save and Quit for me."
:exit - "Exit and save please."
ZZ - "Quit vim."
```

So as you can see, the developers knew that people would get stuck in `vi/vim` so they gave us 37.9 million ways to get out.

Have a great day, and long live `vim`!
