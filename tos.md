---
title: Terms of Service
layout: default
permalink: /tos/
---
<h1 class="headline">{{page.title}}</h1>

This website, <a href="https://exitvim.today">exitvim.today</a>, has a small tracking pixel that solely gives me the following information:

* Browser (Firefox, Chrome, etc) - No version numbers
* Screen Size
* Location (Country level)

Beyond that, I don't track referrers, I do no fingerprinting, nothing. The only reason I use the tracking pixel is because I'm just curious about the number of visits.

I do not use any javascript.

I don't sell any of the data I have, because I don't have any.

Currently this blog is hosted on Gitlab Pages. Gitlab's privacy policy is located [here](https://about.gitlab.com/privacy/). Eventually I'll probably self-host, but until then, this site will remain here.

The tracking pixel is provided by GoatCounter. Their privacy policy is located [here](https://www.goatcounter.com/privacy).

You can use this site anyway you please.
