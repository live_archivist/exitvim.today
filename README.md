# exitvim.today

This is just a simple service to help people get out of `vi` or `vim`.

`exitvi.today` redirects to `exitvim.today`. So if someone is stuck in `vi`, then you can send them that link instead.
